package com.epam.zhuckovich.entity;

/**
 * Enumeration of user types
 */

public enum UserType {
    ADMINISTRATOR, LIBRARIAN, MEMBER
}
